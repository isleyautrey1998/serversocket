#!/usr/bin/env python3.8

import socketserver
import logging
import threading, json, argparse

FORMAT = '%(asctime)s:%(levelname)s:%(process)s:%(message)s'
logging.basicConfig(level=logging.INFO, filename='server.log', filemode='a', format=FORMAT, datefmt='%d-%b-%y %H:%M:%S')

class ForkedJsonHandler(socketserver.BaseRequestHandler):
    '''Created to handle each connection from the client'''
    def handle(self):
        c_ip, c_port = self.client_address
        logging.info(f'-> Connection from {c_ip} via port {c_port}')
        # Use while loop here to have continue server client feedback
        try:
            self.request.sendall(b'\nEnter JSON data: ')
            data = self.request.recv(1024)

            # Validates JSON data
            response = validate_json(data)

            # Tells Client whether data is valid or not
            self.request.sendall(response)
        except Exception as e:
            print(e.message)
        except KeyboardInterrupt:
            logging.warning(f'KeyboardInterrupt while Client [{c_ip}:{c_port}] was still connected!')


class ForkedJsonServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    pass

def validate_json(data):
    # Validate json data here
    data = data.decode().strip()
    try:
        if json.loads(data):
            # Valid JSON Received
            return b'Valid JSON data'
    except ValueError:
        return b'Invalid JSON Data'

def init_parser():
    parser = argparse.ArgumentParser(description='JSON Server that accepts and validates json syntax')
    parser.add_argument('-p', '--port', default=8642,
                        help='Specify the port you want the server to listen on. DEFAULT is 8642')
    return vars(parser.parse_args())

def main():
    args = init_parser()
    port = args['port']
    # Later on-> for key, value in enumerate(args):

    try:
        # Creating a server object with custom Handler
        server = ForkedJsonServer(('', port), ForkedJsonHandler)
        open_port_msg = f'Opening listening port on {port}'
        logging.info(open_port_msg)
        print(open_port_msg)

        # Cuts off Client from server when server closes
        # server.block_on_close = False
        thread = threading.Thread(target=server.serve_forever())
        thread.setDaemon(True)
        thread.start()
    except OSError:
        print(f'\nPort:{port} is currenty being used!')
        print('Select a different port or kill the current process associated to that port\n')
    except KeyboardInterrupt:
        server.shutdown()
        server.server_close()
        logging.info('KeyboardInterrupt - Exiting Server Now\n')

if __name__ == '__main__':
    main()
